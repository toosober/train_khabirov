#include <SFML/Graphics.hpp>
#include <windows.h>

int WINAPI WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow)
{
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);
	
	

    while (window.isOpen())
    {
        window.clear();
        window.draw(shape);
        window.display();
    }

    return 0;
}